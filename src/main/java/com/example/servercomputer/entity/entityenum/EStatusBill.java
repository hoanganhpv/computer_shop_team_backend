package com.example.servercomputer.entity.entityenum;

public enum EStatusBill {
	DRAFT, CONFIRMED, CANCELED
}
