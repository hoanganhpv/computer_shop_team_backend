package com.example.servercomputer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.servercomputer.entity.Bill;
import com.example.servercomputer.entity.User;

@Repository
public interface BillRepository extends JpaRepository<Bill, Long>{

	List<Bill> findByUser(User user);

}
