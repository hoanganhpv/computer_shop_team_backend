package com.example.servercomputer.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.servercomputer.entity.Category;
import com.example.servercomputer.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{
	Page<Product> findByCategory(Category category, Pageable pageable);
//	Page<Product> findAll(Pageable pageable);

	List<Product> findByNameContaining(String keyword);

	List<Product> findAllByCategory(Category category);
}
