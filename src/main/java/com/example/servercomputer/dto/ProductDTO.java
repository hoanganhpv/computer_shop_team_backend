package com.example.servercomputer.dto;

import lombok.Data;

@Data
public class ProductDTO {
	private Long id;
	private String name;
	private String description;
	private Double price;
	private Integer quantity;
	private String image;
	private Long categoryId;
	private String categoryName;
}
