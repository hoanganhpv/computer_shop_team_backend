package com.example.servercomputer.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.servercomputer.dto.UserDTO;
import com.example.servercomputer.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserAPI {
	@Autowired
	private UserService userService;
	
	@GetMapping("/{id}")
	public UserDTO getUser(@PathVariable Long id) {
		return userService.findOneById(id);
	}
	
	@GetMapping
	public List<UserDTO> getAllUser() {
		return userService.findAll();
	}
	
	@PostMapping
	public UserDTO signUp(@Valid @RequestBody UserDTO user) {
		return userService.save(user);
	}
	
//	@PostMapping("/login")
//	public UserDTO login(@RequestBody UserDTO user) {
//		return userService.login(user);
//	}
	
	@PutMapping
	public UserDTO changePassword(@RequestBody UserDTO user) {
		return userService.changePassword(user);
	}
}
