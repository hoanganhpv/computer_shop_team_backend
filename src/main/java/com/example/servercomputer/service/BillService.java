package com.example.servercomputer.service;

import java.util.List;

import com.example.servercomputer.dto.BillDTO;

public interface BillService {

	List<BillDTO> getBillByUser(Long userId);

	BillDTO save(BillDTO bill);

	void deleteBill(Long id);

	List<BillDTO> getAllBill();

}
